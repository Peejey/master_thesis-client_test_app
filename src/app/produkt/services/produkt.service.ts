import { Injectable } from '@angular/core';
import { Produkt } from '../produkt/produkt';
import { ConfigurationService } from '../../configuration.service';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, Subject, ReplaySubject } from 'rxjs';
import { filter, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProduktService {

    actionUrl: string;
    produkte$: Subject<Produkt[]> = new ReplaySubject<Produkt[]>();

    constructor(private http: HttpClient, private configuration: ConfigurationService) {
      this.actionUrl = configuration.produktserviceApi;
      this.aktualisiereProdukte();

    }

    public getProdukte(): Observable<Produkt[]> {
      return this.produkte$.asObservable();
    }

    public aktualisiereProdukte() {
      this.http.get<Produkt[]>(this.actionUrl).subscribe(produkte => {
        this.produkte$.next(produkte);
      });
    }

    public addToWarenkorb(produktId: String) {

    }
}
