export class Produkt {
  id: String;
  name: String;
  beschreibung: String;
  lagerbestand: number;
  preis: number;
}
