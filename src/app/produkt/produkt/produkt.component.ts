import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { MenuItem } from 'primeng/components/common/menuitem';
import { Produkt } from './produkt';
import { WarenkorbService } from '../../warenkorb/services/warenkorb.service';

@Component({
    selector: 'produkt',
    templateUrl: 'produkt.component.html',
    styleUrls: ['produkt.component.scss']
})
export class ProduktComponent implements OnInit, OnChanges {

  @Input()
  produkt: Produkt;

  amount: String = '1';
  defaultAmount: String = '1';

  warenkorbLabel: String = "zum Warenkorb hinzufügen";

  constructor(private warenkorbService: WarenkorbService) {
  }

  ngOnChanges(): void {
  }

  ngOnInit(): void {
  }

  onAddToWarenkorb(event: Event): void {
    this.warenkorbService.addToWarenkorb(this.produkt.id, Number(this.amount));
  }
}
