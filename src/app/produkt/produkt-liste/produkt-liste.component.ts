import { Component, OnInit } from '@angular/core';
import { AccordionModule } from 'primeng/accordion';
import { Produkt } from '../produkt/produkt';
import { ProduktService } from '../services/produkt.service';

@Component({
    selector: 'produkt-liste',
    templateUrl: 'produkt-liste.component.html',
    styleUrls: ['produkt-liste.component.scss']
})
export class ProduktListeComponent implements OnInit{

  produkte: Produkt[];

  constructor(private produktService: ProduktService) {
    this.produktService.getProdukte().subscribe(produkte => {
      this.produkte = produkte;
    });
  }

  ngOnInit(): void {
  }
}
