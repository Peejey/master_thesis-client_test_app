// Angular Imports
import { NgModule } from '@angular/core';

import {AccordionModule} from 'primeng/accordion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SplitButtonModule } from 'primeng/splitbutton';

// This Module's Components
import { ProduktListeComponent } from './produkt-liste/produkt-liste.component';
import { ProduktComponent } from './produkt/produkt.component';
import { PanelModule } from 'primeng/panel';
import { Router } from '@angular/router';
import {DropdownModule} from 'primeng/dropdown';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
      BrowserAnimationsModule, AccordionModule, PanelModule, SplitButtonModule, FormsModule, ReactiveFormsModule,
    ],
    declarations: [
        ProduktListeComponent, ProduktComponent
    ],
    exports: [
        ProduktListeComponent,
    ],
    providers: [{ provide: Router, useValue: {} }]
})
export class ProduktListeModule {

}
