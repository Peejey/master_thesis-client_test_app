export class Artikel {
  id: String;
  name: String;
  menge: number;
  einzelpreis: number;
  gesamtpreis: number;
  referenzId: String;
}
