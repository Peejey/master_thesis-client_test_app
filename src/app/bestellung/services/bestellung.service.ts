import { ConfigurationService } from '../../configuration.service';
import { Warenkorb } from '../../warenkorb/warenkorb';
import { BestellungAnfrage } from '../bestellungAnfrage';
import { Artikel } from '../artikel';
import { HttpClient } from '@angular/common/http';
import { Subject, ReplaySubject, Observable, of } from 'rxjs';
import { Bestellung } from '../bestellung';
import { Injectable } from '@angular/core';
import { BestellungInfos } from '../bestellungInfos';
import { catchError, share } from 'rxjs/operators';
import { ProduktService } from '../../produkt/services/produkt.service';

@Injectable()
export class BestellungService {
  actionUrl: string;
  bestellungDetail$: Subject<Bestellung> = new ReplaySubject<Bestellung>();
  bestellungen$: Subject<Bestellung[]> = new ReplaySubject<Bestellung[]>();
  bestelltEvent$: Subject<boolean> = new ReplaySubject<boolean>();
  geloeschtEvent$: Subject<boolean> = new ReplaySubject<boolean>();

  constructor(private configuration: ConfigurationService, private http: HttpClient) {
    this.actionUrl = configuration.bestellungserviceApi;
    this.http.get<Bestellung[]>(this.actionUrl).subscribe(bestellungen => {
      this.bestellungen$.next(bestellungen);
    });
  }

  public bestellungAnfragen(warenkorb: Warenkorb): Observable<Bestellung> {
    const artikel: Artikel[] = [];
    warenkorb.waren.forEach( ware => {
      artikel.push({
        id: null,
        name: ware.name,
        einzelpreis: ware.preis,
        gesamtpreis: ware.gesamtPreis,
        menge: ware.menge,
        referenzId: ware.id
      });
    });
    const bestellungAnfrage: BestellungAnfrage = {
      warenkorbId: warenkorb.id,
      artikel: artikel
    };
    const response: Observable<Bestellung> = this.http.post<Bestellung>(this.actionUrl, bestellungAnfrage).pipe(
      share()
    );
    response.subscribe(bestellung => {
      this.bestellungDetail$.next(bestellung);
    });
    return response;
  }

  public getBestellungDetail(): Observable<Bestellung> {
    return this.bestellungDetail$.asObservable();
  }

  public getBestellungen(): Observable<Bestellung[]> {
    return this.bestellungen$.asObservable();
  }

  public getBestelltEvent(): Observable<boolean> {
    return this.bestelltEvent$.asObservable();
  }

  public getGeloeschtEvent(): Observable<boolean> {
    return this.geloeschtEvent$.asObservable();
  }

  public bestellen(id: string, bestellungInfos: BestellungInfos): Observable<Bestellung[]> {
    const obs: Observable<Bestellung[]> = this.http.post<Bestellung[]>(this.actionUrl + id + '/bestellen', bestellungInfos).pipe(
      share()
    );
    obs.subscribe(bestellungen => {
      this.bestellungen$.next(bestellungen);
      this.bestelltEvent$.next(true);
    });
    return obs;
  }

  public storniereBestellung(id: string) {
    const obs: Observable<Bestellung[]> = this.http.post<Bestellung[]>(this.actionUrl + id + '/stornieren', null).pipe(
      share()
    );
    obs.subscribe(bestellungen => {
      this.bestellungen$.next(bestellungen);
      this.geloeschtEvent$.next(true);
    });
    return obs;
  }

  public loescheBestellung(id: string) {
    const obs: Observable<Bestellung[]> = this.http.delete<Bestellung[]>(this.actionUrl + id).pipe(
      share()
    );
    obs.subscribe(bestellungen => {
      this.bestellungen$.next(bestellungen);
      this.geloeschtEvent$.next(true);
    });
    return obs;
  }

  public bestellungAnzeigen(id: string) {
    const response: Observable<Bestellung> = this.http.get<Bestellung>(this.actionUrl + id).pipe(
      share()
    );
    response.subscribe(bestellung => {
      this.bestellungDetail$.next(bestellung);
    });
  }

  public aktualisiereBestellungen() {
    this.http.get<Bestellung[]>(this.actionUrl).subscribe(bestellungen => {
      this.bestellungen$.next(bestellungen);
    });
  }
}
