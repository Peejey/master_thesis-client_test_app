// Angular Imports
import { NgModule } from '@angular/core';

// This Module's Components
import { BestellungListeComponent } from './bestellung-liste/bestellung-liste.component';
import { PanelModule, Panel } from 'primeng/panel';
import { AccordionModule } from 'primeng/accordion';
import { BrowserModule } from '@angular/platform-browser';
import { BestellungSummaryComponent } from './bestellung-summary/bestellung-summary.component';
import { ButtonModule } from 'primeng/components/button/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BestellungDetailsComponent } from './bestellung-details/bestellung-details.component';

@NgModule({
    imports: [
      BrowserModule, PanelModule, AccordionModule, ButtonModule, PanelModule, FormsModule, ReactiveFormsModule
    ],
    declarations: [
      BestellungListeComponent, BestellungSummaryComponent, BestellungDetailsComponent
    ],
    exports: [
      BestellungListeComponent, BestellungSummaryComponent, BestellungDetailsComponent
    ]
})
export class BestellungModule {

}
