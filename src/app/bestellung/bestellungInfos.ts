export class BestellungInfos {
  public zahlungsinformation: Zahlungsinformation = new Zahlungsinformation();
  public anschrift: Anschrift = new Anschrift();
}

export class Zahlungsinformation {
  public id: string;
  public iban: string;
  public bic: string;
  public kontoinhaber: string;
  public bank: string;
}

export class Anschrift {
  id: string;
  vorname: string;
  nachname: string;
  strasse: string;
  hausnummer: string;
  plz: string;
  ort: string;
}
