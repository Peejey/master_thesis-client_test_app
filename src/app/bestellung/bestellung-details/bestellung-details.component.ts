import { Component, OnInit } from '@angular/core';
import { Artikel } from '../artikel';
import { WarenkorbService } from '../../warenkorb/services/warenkorb.service';
import { Bestellung } from '../bestellung';
import { BestellungService } from '../services/bestellung.service';
import { BestellungInfos } from '../bestellungInfos';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';

@Component({
    selector: 'bestellung-details',
    templateUrl: 'bestellung-details.component.html',
    styleUrls: ['bestellung-details.component.scss']
})
export class BestellungDetailsComponent implements OnInit {
  bestellung: Bestellung = new Bestellung();
  artikel: Artikel[];
  readOnly = false;

  constructor(private bestellungService: BestellungService, private router: Router, private route: ActivatedRoute,
     private messageService: MessageService) {

    this.bestellungService.getBestellungDetail().subscribe(bestellung => {
      this.bestellung = bestellung;
      this.artikel = bestellung.artikel;
    });
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      let id = params['id'];
      if(!!id) {
        this.bestellungService.bestellungAnzeigen(id);
        this.readOnly = true;
      } else {
        this.readOnly = false;
      }
  });

  }
  get bestellenLabel() {
    return 'Kaufen';
  }

  get abbrechenLabel() {
    return 'Abbrechen';
  }

  get zurueckLabel() {
    return 'zurück';
  }

  onBestellen() {
    const bestellungInfos: BestellungInfos = new BestellungInfos();
    bestellungInfos.anschrift = this.bestellung.anschrift;
    bestellungInfos.zahlungsinformation = this.bestellung.zahlungsinformation;
    this.bestellungService.bestellen(this.bestellung.id, bestellungInfos).subscribe(() => {
      this.router.navigateByUrl('/home');
    }, error => {
      this.messageService.add({severity: 'error', summary: 'Fehler!', detail: error.error.message});
      setTimeout(() => { this.messageService.clear() }, 10000);
      console.log(error);
    });
  }

  onAbbrechen() {
    this.bestellungService.loescheBestellung(this.bestellung.id);
    this.router.navigateByUrl('/home');
  }

  onZurueck() {
    this.router.navigateByUrl('/bestellungen');
  }
}
