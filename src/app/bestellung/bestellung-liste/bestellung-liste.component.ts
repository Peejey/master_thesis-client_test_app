import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BestellungService } from '../services/bestellung.service';
import { Bestellung } from '../bestellung';

@Component({
    selector: 'bestellung-liste',
    templateUrl: 'bestellung-liste.component.html',
    styleUrls: ['bestellung-liste.component.scss']
})
export class BestellungListeComponent {
  bestellungen: Bestellung[];

  constructor(private router: Router, private bestellungService: BestellungService) {
    this.bestellungService.getBestellungen().subscribe(bestellungen => {
      this.bestellungen = bestellungen;
    });
  }

  onLoeschen(bestellung: Bestellung) {
    this.bestellungService.storniereBestellung(bestellung.id);
  }

  getAnzahlArtikelProBestellung(bestellung: Bestellung) {
    let anzahl: number = 0;
    bestellung.artikel.forEach(artikel => {
      anzahl += artikel.menge;
    });
    return anzahl;
  }

  onGoToBestellung(bestellungId: String) {
    this.router.navigate(['/bestellung-details/'], { queryParams: { id: bestellungId } });
    console.log(bestellungId);
  }

  onZurueck() {
    this.router.navigateByUrl('/home');
  }

  get zurueckLabel() {
    return 'zurück';
  }

  getStatus(bestellung): String {
    let status = '';
    if(bestellung.bestellStatus === 'WIRD_VERARBEITET') {
      status = 'IN_ARBEIT';
    }else if(bestellung.bestellStatus === 'FEHLERHAFT_ABGEBROCHEN') {
      status = 'ABGEBROCHEN';
    }else {
      status = bestellung.bestellStatus;
    }
    return status;
  }

  isStorniert(bestellung: Bestellung): boolean {
    return bestellung.bestellStatus === 'STORNIERT';
  }

  isWirdVerarbeitet(bestellung: Bestellung): boolean {
    return bestellung.bestellStatus === 'WIRD_VERARBEITET';
  }

  isFehlerhaftAbgebrochen(bestellung: Bestellung): boolean {
    return bestellung.bestellStatus === 'FEHLERHAFT_ABGEBROCHEN';
  }

  isBestellt(bestellung: Bestellung): boolean {
    return bestellung.bestellStatus === 'BESTELLT';
  }

}
