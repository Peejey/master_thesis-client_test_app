import { Artikel } from './artikel';

export class BestellungAnfrage {
  public warenkorbId: String;
  public artikel: Artikel[];
}
