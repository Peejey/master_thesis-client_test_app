import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BestellungService } from '../services/bestellung.service';
import { Bestellung } from '../bestellung';

@Component({
    selector: 'bestellung-summary',
    templateUrl: 'bestellung-summary.component.html',
    styleUrls: ['bestellung-summary.component.scss']
})
export class BestellungSummaryComponent {

  bestellungen: Bestellung[];
  bestellungenAnzahl = 0;
  bestellungenGesamtsumme = 0;
  bestellungenArtikelAnzahl = 0;

  constructor(private router: Router, private bestellungService: BestellungService) {
    this.bestellungService.getBestellungen().subscribe(bestellungen => {
      bestellungen = bestellungen;
      if (!!bestellungen) {
        this.bestellungenAnzahl = bestellungen.length;
        let summe = 0;
        let anzahlArtikel = 0;
        bestellungen.forEach( bestellung => {
          summe += bestellung.gesamtsumme;
          bestellung.artikel.forEach(artikel => {
            anzahlArtikel += artikel.menge;
          });
        });
        this.bestellungenGesamtsumme = summe;
        this.bestellungenArtikelAnzahl = anzahlArtikel;
      }
    });
  }

  get bestellungLabel() {
    return 'Bestellung ansehen';
  }

  onGoToBestellung() {
    this.router.navigateByUrl('/bestellungen');
  }

}
