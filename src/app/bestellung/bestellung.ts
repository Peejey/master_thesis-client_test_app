import { Artikel } from './artikel';
import { Zahlungsinformation, Anschrift } from './bestellungInfos';

export class Bestellung {

  public id: string;
  public bestellDatum: string;
  public artikel: Artikel[];
  public gesamtsumme: number;
  public versandkosten: number;
  public mwst: number;
  public artikelSumme: number;
  public zahlungsinformation: Zahlungsinformation = new Zahlungsinformation();
  public anschrift: Anschrift = new Anschrift();
  public bestellStatus: string;
}
