import { Ware } from './Ware';

export class Warenkorb {
  public id: String;
  public gesamtPreis: number;
  public warenAnzahl: number;
  public waren: Ware[];
}
