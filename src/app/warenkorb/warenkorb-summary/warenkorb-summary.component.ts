import { Component, OnInit, OnChanges } from '@angular/core';
import { Produkt } from 'src/app/produkt/produkt/produkt';
import { SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { WarenkorbService } from '../services/warenkorb.service';
import { Ware } from '../Ware';
import { Observable } from 'rxjs';
import { Input } from '@angular/core';
import { Warenkorb } from '../warenkorb';
import { filter } from 'rxjs/operators';

@Component({
    selector: 'warenkorb-summary',
    templateUrl: 'warenkorb-summary.component.html',
    styleUrls: ['warenkorb-summary.component.scss']
})
export class WarenkorbSummaryComponent implements OnInit, OnChanges{
  warenkorb: Warenkorb;
  warenAnzahl = 0;
  gesamtPreis = 0;

  constructor(private router: Router, warenkorbService: WarenkorbService){
    warenkorbService.getWarenkorb().pipe(filter(x => !!x)).subscribe( (warenkorb: Warenkorb) => {
      this.warenkorb = warenkorb;
      this.warenAnzahl = warenkorb.warenAnzahl;
      this.gesamtPreis = warenkorb.gesamtPreis;
    });
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  redirectToWarenkorb() {
    this.router.navigateByUrl('/warenkorb');
  }

  get warenkorbLabel() {
    return 'Warenkorb ansehen';
  }

  onGoToWarenkorb() {
    this.router.navigateByUrl('/warenkorb');
  }

}
