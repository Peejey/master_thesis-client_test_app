// Angular Imports
import { NgModule } from '@angular/core';

// This Module's Components
import { WarenkorbListeComponent } from './warenkorb-liste/warenkorb-liste.component';
import { PanelModule } from 'primeng/components/panel/panel';
import { AccordionModule } from 'primeng/accordion';
import { WarenkorbSummaryComponent } from './warenkorb-summary/warenkorb-summary.component';
import { BrowserModule } from '@angular/platform-browser';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
      BrowserModule, PanelModule, AccordionModule, ButtonModule, FormsModule, ReactiveFormsModule,
    ],
    declarations: [
      WarenkorbListeComponent, WarenkorbSummaryComponent
    ],
    exports: [
      WarenkorbListeComponent, WarenkorbSummaryComponent
    ]
})
export class WarenkorbModule {

}
