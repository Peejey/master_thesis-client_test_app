import { Injectable } from '@angular/core';
import { Produkt } from '../../produkt/produkt/produkt';
import { Ware } from '../Ware';
import { Observable, of, from, Subject, BehaviorSubject, ReplaySubject } from 'rxjs';
import { Warenkorb } from '../warenkorb';
import { ConfigurationService } from '../../configuration.service';
import { HttpClient } from '@angular/common/http';
import { WareAnfrage } from '../wareAnfrage';

@Injectable()
export class WarenkorbService {

    actionUrl: string;
    warenkorb$:  BehaviorSubject<Warenkorb> = new BehaviorSubject<Warenkorb>(null);


    constructor(private http: HttpClient, private configuration: ConfigurationService) {
      this.actionUrl = configuration.warenkorbserviceApi;
      this.http.post<Warenkorb>(this.actionUrl, null).subscribe( w => {
        this.warenkorb$.next(w);
      });
    }

    public addToWarenkorb(wareId: String, warenMenge: number) {
      const anfrage: WareAnfrage = { id: wareId, menge: warenMenge };
      this.http.post<Warenkorb>(this.actionUrl + this.warenkorb$.value.id, anfrage).subscribe( w => {
        this.warenkorb$.next(w);
      });
    }

    public removeFromWarenkorb(wareId: String) {
      this.http.delete<Warenkorb>(this.actionUrl + this.warenkorb$.value.id + '/' + wareId).subscribe( w => {
        this.warenkorb$.next(w);
      });
    }

    public getWarenkorb(): Observable<Warenkorb> {
      return this.warenkorb$.asObservable();
    }

    public mengeAendern(wareId: String, warenMenge: number) {
      this.http.put<Warenkorb>(this.actionUrl + this.warenkorb$.value.id + '/' + wareId, warenMenge).subscribe( w => {
        this.warenkorb$.next(w);
      });
    }

    public getLatestValue(): Warenkorb {
      return this.warenkorb$.value;
    }

    public aktualisiereWarenkorb() {
      this.http.get<Warenkorb>(this.actionUrl + this.warenkorb$.value.id).subscribe( w => {
        this.warenkorb$.next(w);
      });
    }

    round(number, precision) {
      let factor = Math.pow(10, precision);
      let tempNumber = number * factor;
      let roundedTempNumber = Math.round(tempNumber);
      return roundedTempNumber / factor;
    }
}
