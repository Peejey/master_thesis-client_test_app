
export class Ware {
  id: String;
  name: String;
  menge: number;
  preis: number;
  gesamtPreis: number;
}
