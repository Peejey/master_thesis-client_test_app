import { Component, OnInit } from '@angular/core';
import { Produkt } from '../../produkt/produkt/produkt';
import { Observable } from 'rxjs';
import { Ware } from '../Ware';
import { WarenkorbService } from '../services/warenkorb.service';
import { Warenkorb } from '../warenkorb';
import { Router } from '@angular/router';
import { BestellungService } from '../../bestellung/services/bestellung.service';
import {MessageService} from 'primeng/api';
import { timeout } from 'rxjs/operators';

@Component({
    selector: 'warenkorb-liste',
    templateUrl: 'warenkorb-liste.component.html',
    styleUrls: ['warenkorb-liste.component.scss']
})
export class WarenkorbListeComponent implements OnInit {

  warenkorb: Warenkorb;

  constructor(private warenkorbService: WarenkorbService, private bestellungService: BestellungService,
    private router: Router, private messageService: MessageService) {
    warenkorbService.getWarenkorb().subscribe( (warenkorb: Warenkorb) => {
      this.warenkorb = warenkorb;
    });
  }

  ngOnInit(): void {
  }

  get bestellenLabel() {
    return 'Bestellung aufgeben';
  }

  get mengeAendernLabel() {
    return 'ändern';
  }

  get zurueckLabel() {
    return 'zurück';
  }

  onBestellen() {
    this.bestellungService.bestellungAnfragen(this.warenkorb).subscribe( value => {
      this.router.navigateByUrl('/bestellung-details');
    }, error => {
      this.messageService.add({severity: 'error', summary: 'Fehler!', detail: error.error.message});
      setTimeout(() => { this.messageService.clear() }, 10000);
      console.log(error);
    });
  }

  onMengeAendern(ware: Ware) {
    this.warenkorbService.mengeAendern(ware.id, Number(ware.menge));
  }

  onLoeschen(ware: Ware) {
    this.warenkorbService.removeFromWarenkorb(ware.id);
  }

  onZurueck() {
    this.router.navigateByUrl('/home');
  }

  showBestellungButton() {
    return !!this.warenkorb && this.warenkorb.waren.length > 0;
  }
}
