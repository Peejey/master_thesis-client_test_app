import { Component } from '@angular/core';
import { WarenkorbService } from '../warenkorb/services/warenkorb.service';
import { Ware } from '../warenkorb/Ware';
import { HomeService } from './home.service';

@Component({
    selector: 'home',
    templateUrl: 'home.component.html',
    styleUrls: ['home.component.scss']
})
export class HomeComponent {

  constructor(private homeService: HomeService){
  }

}
