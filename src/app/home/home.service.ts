import { Injectable } from '@angular/core';
import { ProduktService } from '../produkt/services/produkt.service';
import { WarenkorbService } from '../warenkorb/services/warenkorb.service';
import { BestellungService } from '../bestellung/services/bestellung.service';
import { ZahlungService } from '../zahlung/services/zahlung.service';

@Injectable()
export class HomeService {

  constructor(private produktService: ProduktService, private warenkorbService: WarenkorbService,
    private bestellungService: BestellungService, private zahlungService: ZahlungService) {
    this.bestellungService.getBestelltEvent().subscribe(action => {
      this.produktService.aktualisiereProdukte();
      this.warenkorbService.aktualisiereWarenkorb();
      this.zahlungService.aktualisiereZahlungen();
    });
    this.bestellungService.getGeloeschtEvent().subscribe(action => {
      this.produktService.aktualisiereProdukte();
      this.zahlungService.aktualisiereZahlungen();
    });
  }

  public refreshAll() {
    this.produktService.aktualisiereProdukte();
    this.warenkorbService.aktualisiereWarenkorb();
    this.zahlungService.aktualisiereZahlungen();
    this.bestellungService.aktualisiereBestellungen();
  }
}
