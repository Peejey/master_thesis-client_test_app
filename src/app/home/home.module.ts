// Angular Imports
import { NgModule } from '@angular/core';

// This Module's Components
import { HomeComponent } from './home.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BestellungModule } from '../bestellung/bestellung.module';
import { ProduktListeModule } from '../produkt/produkt.module';
import { WarenkorbModule } from '../warenkorb/warenkorb.module';
import {CardModule} from 'primeng/card';
import { ZahlungModule } from '../zahlung/zahlung.module';

@NgModule({
    imports: [
      BrowserModule, BrowserAnimationsModule,
      ProduktListeModule, WarenkorbModule, BestellungModule, ZahlungModule
    ],
    declarations: [
        HomeComponent,
    ],
    exports: [
        HomeComponent,
    ]
})
export class HomeModule {

}
