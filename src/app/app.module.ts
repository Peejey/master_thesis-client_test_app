import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BestellungModule } from './bestellung/bestellung.module';
import { ProduktListeModule } from './produkt/produkt.module';
import { WarenkorbModule } from './warenkorb/warenkorb.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccordionModule } from 'primeng/accordion';
import { Routes } from '@angular/router';
import { ProduktComponent } from './produkt/produkt/produkt.component';
import { RouterModule } from '@angular/router';
import { WarenkorbListeComponent } from './warenkorb/warenkorb-liste/warenkorb-liste.component';
import { ProduktListeComponent } from './produkt/produkt-liste/produkt-liste.component';
import { CardModule } from 'primeng/components/card/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { HomeModule } from './home/home.module';
import { WarenkorbService } from './warenkorb/services/warenkorb.service';
import { ConfigurationService } from './configuration.service';
import { HttpClientModule } from '@angular/common/http';
import { ProduktService } from './produkt/services/produkt.service';
import { BestellungListeComponent } from './bestellung/bestellung-liste/bestellung-liste.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BestellungDetailsComponent } from './bestellung/bestellung-details/bestellung-details.component';
import { BestellungService } from './bestellung/services/bestellung.service';
import { MessageService } from 'primeng/api';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import { HomeService } from './home/home.service';
import { ZahlungModule } from './zahlung/zahlung.module';
import { ZahlungListeComponent } from './zahlung/zahlung-liste/zahlung-liste.component';
import { ZahlungService } from './zahlung/services/zahlung.service';

const appRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent},
  { path: 'warenkorb', component: WarenkorbListeComponent },
  { path: 'produkte', component: ProduktListeComponent },
  { path: 'bestellung-details', component: BestellungDetailsComponent },
  { path: 'bestellung-details/:id', component: BestellungDetailsComponent },
  { path: 'bestellungen', component: BestellungListeComponent },
  { path: 'zahlungen', component: ZahlungListeComponent },
  { path: '**', redirectTo: 'home' }
];


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, BrowserAnimationsModule, CardModule, HttpClientModule, FontAwesomeModule, MessageModule, MessagesModule,
    HomeModule, BestellungModule, ProduktListeModule, WarenkorbModule, ZahlungModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  providers: [WarenkorbService, ConfigurationService, ProduktService, BestellungService, MessageService, HomeService, ZahlungService],
  bootstrap: [AppComponent]
})
export class AppModule { }
