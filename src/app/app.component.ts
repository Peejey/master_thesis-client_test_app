import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { HomeService } from './home/home.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'microservice-test-app';

  constructor(private router: Router, private homeService: HomeService) {
  }

  redirectToHome() {
    this.router.navigateByUrl('/home');
    this.homeService.refreshAll();
  }
}
