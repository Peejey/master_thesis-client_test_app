import { Injectable } from '@angular/core';

@Injectable()
export class ConfigurationService {
    public produktserviceBaseUrl = 'http://localhost:8010/';
    public produktApi = 'produkte/';
    public warenkorbserviceBaseUrl = 'http://localhost:8011/';
    public warenkorbApi = 'warenkorb/';
    public bestellungserviceBaseUrl = 'http://localhost:8012/';
    public bestellungApi = 'bestellungen/';
    public zahlungserviceBaseUrl = 'http://localhost:8013/';
    public zahlungApi = 'zahlung/';

    public produktserviceApi = this.produktserviceBaseUrl + this.produktApi;
    public warenkorbserviceApi = this.warenkorbserviceBaseUrl + this.warenkorbApi;
    public bestellungserviceApi = this.bestellungserviceBaseUrl + this.bestellungApi;
    public zahlungserviceApi = this.zahlungserviceBaseUrl + this.zahlungApi;
}
