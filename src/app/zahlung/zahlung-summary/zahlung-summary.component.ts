import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ZahlungService } from '../services/zahlung.service';
import { Zahlung } from '../zahlung';
import { filter } from 'rxjs/operators';

@Component({
    selector: 'zahlung-summary',
    templateUrl: 'zahlung-summary.component.html',
    styleUrls: ['zahlung-summary.component.scss']
})
export class ZahlungSummaryComponent {

  zahlungen: Zahlung[];
  anzahlZahlungen: number;
  anzahlStornierungen: number;

  constructor(private router: Router, private zahlungService: ZahlungService){
    this.zahlungService.getZahlungen().pipe(
      filter( x => !!x)
    ).subscribe(zahlungen => {
      this.zahlungen = zahlungen;
      this.anzahlZahlungen = zahlungen.length;
      let storno = 0;
      zahlungen.forEach( zahlung => {
        if(zahlung.storniert){
          storno++;
        }
      });
      this.anzahlStornierungen = storno;
    });
  }

  redirectToZahlung() {
    this.router.navigateByUrl('/zahlungen');
  }

  get zahlungLabel() {
    return 'Zahlungen ansehen';
  }

  onGoToZahlung() {
    this.router.navigateByUrl('/zahlungen');
  }

}
