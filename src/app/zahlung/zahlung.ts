import { Zahlungsinformation } from '../bestellung/bestellungInfos';

export class Zahlung {
  public id: string;
  public zahlungsinformation: Zahlungsinformation;
  public zahlungssumme: number;
  public referenzId: string;
  public bezahlt: boolean;
  public bezahltTimestamp: string;
  public storniert: boolean;
  public storniertTimestamp: string;
}
