// Angular Imports
import { NgModule } from '@angular/core';

// This Module's Components
import { ZahlungListeComponent } from './zahlung-liste/zahlung-liste.component';
import { ZahlungSummaryComponent } from './zahlung-summary/zahlung-summary.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { AccordionModule } from 'primeng/accordion';
import { PanelModule } from 'primeng/panel';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
    imports: [
      BrowserModule, PanelModule, AccordionModule, ButtonModule, FormsModule, ReactiveFormsModule
    ],
    declarations: [
      ZahlungListeComponent, ZahlungSummaryComponent
    ],
    exports: [
      ZahlungListeComponent, ZahlungSummaryComponent
    ]
})
export class ZahlungModule {

}
