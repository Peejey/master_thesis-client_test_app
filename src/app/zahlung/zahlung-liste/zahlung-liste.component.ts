import { Component } from '@angular/core';
import { Zahlung } from '../zahlung';
import { ZahlungService } from '../services/zahlung.service';
import { Router } from '@angular/router';

@Component({
    selector: 'zahlung-liste',
    templateUrl: 'zahlung-liste.component.html',
    styleUrls: ['zahlung-liste.component.scss']
})
export class ZahlungListeComponent {
  zahlungen: Zahlung[];

  constructor(private zahlungService: ZahlungService, private router: Router) {
    zahlungService.getZahlungen().subscribe( (zahlungen: Zahlung[]) => {
      this.zahlungen = zahlungen;
    });
  }

  get zurueckLabel() {
    return 'zurück';
  }

  onZurueck() {
    this.router.navigateByUrl('/home');
  }

  onGoToBestellung(bestellungId: String) {
    this.router.navigate(['/bestellung-details/'], { queryParams: { id: bestellungId } });
    console.log(bestellungId);
  }
}
