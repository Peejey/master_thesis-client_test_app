import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ConfigurationService } from '../../configuration.service';
import { HttpClient } from '@angular/common/http';
import { Zahlung } from '../zahlung';

@Injectable()
export class ZahlungService {
  actionUrl: string;
  zahlungen$:  BehaviorSubject<Zahlung[]> = new BehaviorSubject<Zahlung[]>(null);

  constructor(private http: HttpClient, private configuration: ConfigurationService) {
    this.actionUrl = configuration.zahlungserviceApi;
    this.aktualisiereZahlungen();
  }

  getZahlungen(): Observable<Zahlung[]> {
    return this.zahlungen$.asObservable();
  }

  public aktualisiereZahlungen() {
    this.http.get<Zahlung[]>(this.actionUrl).subscribe( w => {
      this.zahlungen$.next(w);
    });
  }
}
